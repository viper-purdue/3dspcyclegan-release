<!---
#Readme for 3DSpCycleGAN Package 02/11/2023
#Copyright 2022 - The Board of Trustees of Purdue University - All rights reserved
#This is the training and inference code for 3DSpCycleGAN.

#This software is covered by US patents and copyright.
#This source code is to be used for academic research purposes only, and no commercial use is allowed.
-->

*****
# Term of Use and License
Version 1.1
February 1, 2023


This work was partially supported by a George M. O’Brien Award from the National Institutes of Health 
under grant NIH/NIDDK P30 DK079312 and the endowment of the Charles William Harrison Distinguished 
Professorship at Purdue University.

Copyright and Intellectual Property

The software/code and data are distributed under Creative Commons license
Attribution-NonCommercial-ShareAlike - CC BY-NC-SA

You are free to:
* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material
* The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. (See below for paper citation)
* NonCommercial — You may not use the material for commercial purposes.
* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For more information see:
https://creativecommons.org/licenses/by-nc-sa/4.0/


The data is distributed under Creative Commons license
Attribution-NonCommercial-NoDerivs - CC BY-NC-ND

You are free to:
* Share — copy and redistribute the material in any medium or format
* The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* NonCommercial — You may not use the material for commercial purposes.
* NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For More Information see:
https://creativecommons.org/licenses/by-nc-nd/4.0/



Attribution
In any publications you produce based on using our software/code or data we ask that you cite the following paper:
```BibTeX
@ARTICLE{wu2023SpCycleGAN3D,
  author={Liming Wu, Alain Chen, Paul Salama, Kenneth W. Dunn, and Edward J. Delp},
  title={Three-Dimensional Nuclei Synthesis For Fluorescence Microscopy Image Analysis},
  Journal={Proceedings of the IEEE International Symposium on Biomedical Imaging (ISBI)},
  year={2023},
  month={April},
}
```

Privacy Statement
We are committed to the protection of personal privacy and have adopted a policy to  protect information about individuals. 
When using our software/source code we do not collect any information about you or your use of the code.

How to Contact Us

The source code/software and data is distributed "as is." 
We do not accept any responsibility for the operation of the software.
We can only provided limited help in running our code. 
Note: the software/code developed under Linux. 
We do not do any work with the Windows OS and cannot provide much help in running our software/code on the Windows OS

If you have any questions contact: imart@ecn.purdue.edu

*****

# 3DSpCycleGAN Training and Inference
This repository contains **training**, **testing**, and **inference** code for 3DSpCycleGAN.

## Installation
This project is built using [PyTorch](https://pytorch.org), a high-level library for Deep Learning.

Note: This package has been developed and compiled to run on a Linux machine and will not work with on any other machine that is running a different OS such Windows OS.
 

### Software installation requirements:
1. One NVIDIA A40 GPU with 48GB memory is recommended to run this project (Batch size = 1 during training requires ~24GB memory)
2. Use the command `conda env create -f environment.yml` to create a virtual environment with required packages
3. Install [PyTorch](https://pytorch.org/get-started/locally/) based on your CUDA version
5. Run `sh scripts/download.sh` to download the provided example datasets and pretrained models. This will create a `checkpoints` directory and a `datasets` directory

## Repository Structure 
This repository is structured in the following way: 
* `scripts/`: bash files for training and inference
* `checkpoints/`: trained models
* `data/`: data loader files for loading and preprocessing volumes before sending to the network
* `result/`:example images generated when inferecing the provided pre-trained models
* `environment.yml`: virtual environment packages needed to install
* `utils/`: utility functions
* `options/`: traning and testing tunnable parameters
* `dataset/`: example datasets used for training, testing, and inference
* `models/`: defination of network architectures
* `outputs/`: log output generates from bash
* `train.py`: training script
* `test.py`: inference script

## Dataset
Some example datasets are provided as in `datasets/`, if you had run `sh scripts/download.sh`. 

## How to use the code

### Training
The example training data is provided in `datasets/`. 
Take `D1_unpair` dataset as an example:
1. `trainA`: synthetic binary segmentation masks for training
2. `trainB`: real microscopy subvolumes for training
3. `testA`: synthetic binary segmentation masks for testing (not used)
4. `testB`: real microscopy subvolumes for testing (not used)
5. `group1 - group4`: synthetic binary segmentation masks for inference
To train on example data:
1. Make appropriate modifications in `options/` based on your GPUs and other training parameters
2. Run command `sh scripts/train3d128.sh`, and the new weights will be saved in `checkpoints/`

### Training on your own data:

If you are using your own data, please mimic the training data settings in `options/` and directory structures in `datasets`
1. Organize your own data with the same structure as the example training data under `datasets/3d128`
2. Make appropriate modifications in`options/` based on your GPUs and other training parameters
3. Run command `sh scripts/train3d128.sh`, and the new weights will be saved in `checkpoints`

### Using trained models

If you want to generate some synthetic microscopy volumes using the trained model, you can do **inference**:
2. Make sure other settings are correct in the files under `options/`
3. Make appropriate changes in `scripts/test3d128.sh` 
4. Run command `sh scripts/test3d128.sh`. This will save detected results on each slice and save to `results/`


# References
C. Fu, S. Lee, D. J. Ho, S. Han, P. Salama, K. W. Dunn, and E. J. Delp, "Three Dimensional Fluorescence Microscopy Image Synthesis and Segmentation," Proceedings of the Computer Vision for Microscopy Image Analysis (CVMI) Workshop at Computer Vision and Pattern Recognition (CVPR), June 2018. DOI: 10.1109/CVPRW.2018.00298
