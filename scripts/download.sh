# download example datasets
wget https://skynet.ecn.purdue.edu/~micro/3DSpCycleGAN_release/datasets.zip --no-check-certificate
unzip datasets.zip
# download trained weights
wget https://skynet.ecn.purdue.edu/~micro/3DSpCycleGAN_release/checkpoints.zip --no-check-certificate
unzip checkpoints.zip