# wsm
nohup python train.py --dataroot ./datasets/3d128/D1_unpair \
                --name D1_unpair --model attention_gan3d \
                --dataset_mode unaligned3d --no_dropout --lambda_A 10 --lambda_B 10 \
                --lambda_identity 0.5 --display_id 1 --display_freq 10 --print_freq 10 \
                --batch_size 1 >> ./outputs/D1_unpair.txt &

# immu
nohup python train.py --dataroot ./datasets/3d128/D2_unpair \
                --name D2_unpair --model attention_gan3d \
                --dataset_mode unaligned3d --no_dropout --lambda_A 10 --lambda_B 10 \
                --lambda_identity 0.5 --display_id 1 --display_freq 10 --print_freq 10 \
                --batch_size 1 >> ./outputs/D2_unpair.txt &