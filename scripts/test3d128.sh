# wsm
python test.py --dataroot ./datasets/3d128/D1_unpair/group1/ \
                --name D1_unpair --model attention_gan3d \
                --dataset_mode unaligned3d --norm instance \
                --phase test --no_dropout --load_size 128 \
                --crop_size 128 --batch_size 1 --gpu_ids 0 \
                --saveDisk --direction AtoB --epoch 185 --num_test 50

# nohup python test.py --dataroot ./datasets/3d128/D1_unpair/group2/ \
#                 --name D1_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 150 --num_test 50 &

# nohup python test.py --dataroot ./datasets/3d128/D1_unpair/group3/ \
#                 --name D1_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 145 --num_test 50 &

# nohup python test.py --dataroot ./datasets/3d128/D1_unpair/group4/ \
#                 --name D1_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 135 --num_test 50 &

# # immu
# nohup python test.py --dataroot ./datasets/3d128/D2_unpair/group1/ \
#                 --name D2_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 205 --num_test 50 &

# nohup python test.py --dataroot ./datasets/3d128/D2_unpair/group2/ \
#                 --name D2_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 200 --num_test 50 &

# nohup python test.py --dataroot ./datasets/3d128/D2_unpair/group3/ \
#                 --name D2_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 185 --num_test 50 &

# nohup python test.py --dataroot ./datasets/3d128/D2_unpair/group4/ \
#                 --name D2_unpair --model attention_gan3d \
#                 --dataset_mode unaligned3d --norm instance \
#                 --phase test --no_dropout --load_size 128 \
#                 --crop_size 128 --batch_size 1 --gpu_ids 0 \
#                 --saveDisk --direction AtoB --epoch 130 --num_test 50 &