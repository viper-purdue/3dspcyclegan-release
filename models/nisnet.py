#########################################################################################
# Copyright 2020 The Board of Trustees of Purdue University and the Purdue Research Foundation. All rights reserved.
# Script for demo the models. 
# Usage: python train.py 
# Author: purdue micro team
# Date: 1/17/2020
#########################################################################################

import torch.nn as nn
import torch.nn.functional as F
import torch

def conv_block(in_dim,out_dim,act_fn):
    model = nn.Sequential(
        nn.Conv3d(in_dim,out_dim, kernel_size=3, stride=1, padding=1),
        nn.BatchNorm3d(out_dim),
        act_fn,
    )
    return model


def conv_trans_block(in_dim,out_dim,act_fn):
    model = nn.Sequential(
        nn.ConvTranspose3d(in_dim,out_dim, kernel_size=3, stride=2, padding=1,output_padding=1),
        nn.BatchNorm3d(out_dim),
        act_fn,
    )
    return model

def build_conv_block(dim):
    conv_block = []
    conv_block += [nn.Conv3d(dim, dim, kernel_size=3, stride=1, padding=1),
                   nn.BatchNorm3d(dim),
                   nn.LeakyReLU(0.2, inplace=True)]

    conv_block += [nn.Conv3d(dim, dim, kernel_size=3, stride=1, padding=1),
                   nn.BatchNorm3d(dim)]

    return nn.Sequential(*conv_block)

    

def maxpool():
    pool = nn.MaxPool3d(kernel_size=2, stride=2, padding=0)
    return pool


def conv_block_2(in_dim,out_dim,act_fn):
    model = nn.Sequential(
        conv_block(in_dim,out_dim,act_fn),
        nn.Conv3d(out_dim,out_dim, kernel_size=3, stride=1, padding=1),
        nn.BatchNorm3d(out_dim),
    )
    return model    


def conv_block_3(in_dim,out_dim,act_fn):
    model = nn.Sequential(
        conv_block(in_dim,out_dim,act_fn),
        conv_block(out_dim,out_dim,act_fn),
        nn.Conv3d(out_dim,out_dim, kernel_size=3, stride=1, padding=1),
        nn.BatchNorm3d(out_dim),
    )
    return model

def conv_block_twice(in_dim,out_dim,act_fn):
    model = nn.Sequential(
        conv_block(in_dim,out_dim,act_fn),
        conv_block(out_dim,out_dim,act_fn),
    )
    return model

class Residual_block(nn.Module):
    def __init__(self,ch_in, ch_out):
        super(Residual_block,self).__init__()
        self.ch_out = ch_out
        self.ch_in = ch_in
        self.conv_1x1 = nn.Conv3d(ch_in,ch_out,kernel_size=1,stride=1,padding=0)
        self.conv1 = nn.Sequential(
            nn.Conv3d(ch_out,ch_out,kernel_size=3,stride=1,padding=1,bias=True),
            nn.BatchNorm3d(ch_out),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.conv2 = nn.Sequential(
            nn.Conv3d(ch_out,ch_out,kernel_size=3,stride=1,padding=1,bias=True),
            nn.BatchNorm3d(ch_out),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.relu = nn.LeakyReLU(0.2, inplace=True)

    def forward(self,x):
        x = self.conv_1x1(x)
        x1 = self.conv1(x)
        x2 = self.conv2(x1)
        y = self.relu(x+x2)
        return y

class Attention_block(nn.Module):
    def __init__(self,F_g,F_l,F_int):
        super(Attention_block,self).__init__()
        self.W_g = nn.Sequential(
            nn.Conv3d(F_g, F_int, kernel_size=1,stride=1,padding=0,bias=True),
            nn.BatchNorm3d(F_int)
            )
        
        self.W_x = nn.Sequential(
            nn.Conv3d(F_l, F_int, kernel_size=1,stride=1,padding=0,bias=True),
            nn.BatchNorm3d(F_int)
        )

        self.psi = nn.Sequential(
            nn.Conv3d(F_int, 1, kernel_size=1,stride=1,padding=0,bias=True),
            nn.BatchNorm3d(1),
            nn.Sigmoid()
        )
        
        self.relu = nn.LeakyReLU(0.2, inplace=True)
        
    def forward(self,g,x):
        g1 = self.W_g(g)
        x1 = self.W_x(x)
        psi = self.relu(g1+x1)
        psi = self.psi(psi)

        return x*psi

class NISNet(nn.Module):
	def __init__(self, in_dim, out_dim, num_filter):
		super(NISNet, self).__init__()
		self.in_dim = in_dim
		self.out_dim = out_dim
		self.num_filter = num_filter
		act_fn = nn.LeakyReLU(0.2, inplace=True)

		print("\n--------Initialing ResAttUNet---------\n")

		self.conv1 = conv_block_twice(self.in_dim,self.num_filter,act_fn)
		self.pool1 = nn.Conv3d(self.num_filter,self.num_filter,kernel_size=2,stride=2,padding=0)
		# self.pool1 = maxpool()
		self.conv2 = Residual_block(self.num_filter*1,self.num_filter*2)
		# self.pool2 = maxpool()
		self.pool2 = nn.Conv3d(self.num_filter*2,self.num_filter*2,kernel_size=2,stride=2,padding=0)
		self.conv3 = Residual_block(self.num_filter*2,self.num_filter*4)
		# self.pool3 = maxpool()
		self.pool3 = nn.Conv3d(self.num_filter*4,self.num_filter*4,kernel_size=2,stride=2,padding=0)
		self.conv4 = Residual_block(self.num_filter*4,self.num_filter*8)
		# self.pool4 = maxpool()
		self.pool4 = nn.Conv3d(self.num_filter*8,self.num_filter*8,kernel_size=2,stride=2,padding=0)
	
		self.conv5 = Residual_block(self.num_filter*8,self.num_filter*16)

		self.up1 = conv_trans_block(self.num_filter*16,self.num_filter*8,act_fn)
		self.att1 = Attention_block(F_g=self.num_filter*8,F_l=self.num_filter*8,F_int=self.num_filter*8)
		self.conv6 = Residual_block(self.num_filter*16,self.num_filter*8)

		self.up2 = conv_trans_block(self.num_filter*8,self.num_filter*4,act_fn)
		self.att2 = Attention_block(F_g=self.num_filter*4,F_l=self.num_filter*4,F_int=self.num_filter*4)
		self.conv7 = Residual_block(self.num_filter*8,self.num_filter*4)

		self.up3 = conv_trans_block(self.num_filter*4,self.num_filter*2,act_fn)
		self.att3 = Attention_block(F_g=self.num_filter*2,F_l=self.num_filter*2,F_int=self.num_filter*2)
		self.conv8 = Residual_block(self.num_filter*4,self.num_filter*2)

		self.up4 = conv_trans_block(self.num_filter*2,self.num_filter*1,act_fn)
		self.att4 = Attention_block(F_g=self.num_filter,F_l=self.num_filter,F_int=self.num_filter)

		self.conv9 = conv_block_twice(self.num_filter*2,self.num_filter*1,act_fn)
		self.conv10 = conv_block_twice(self.num_filter*2,self.num_filter*1,act_fn)

		self.conv_cls = nn.Sequential(
			nn.Conv3d(self.num_filter,out_channels=1, kernel_size=3, stride=1, padding=1),
			# nn.Sigmoid(),
            nn.Tanh()
		)
		# self.conv_vec = nn.Sequential(
		# 	nn.Conv3d(self.num_filter,out_channels=3,kernel_size=3, stride=1, padding=1)
		# )

	def forward(self, x):
		x1 = self.conv1(x)
		p1 = self.pool1(x1)
		x2 = self.conv2(p1)
		p2 = self.pool2(x2)
		x3 = self.conv3(p2)
		p3 = self.pool3(x3)
		x4 = self.conv4(p3)
		p4 = self.pool4(x4)

		x5 = self.conv5(p4)

		u1 = self.up1(x5)
		x4 = self.att1(g=u1, x=x4)
		c1 = torch.cat([x4,u1],dim=1)

		x6 = self.conv6(c1)
		u2 = self.up2(x6)
		x3 = self.att2(g=u2, x=x3)
		c2 = torch.cat([x3,u2],dim=1)

		x7 = self.conv7(c2)
		u3 = self.up3(x7)
		x2 = self.att3(g=u3, x=x2)
		c3 = torch.cat([x2,u3],dim=1)

		x8 = self.conv8(c3)
		u4 = self.up4(x8)
		x1 = self.att4(g=u4,x=x1)
		c4 = torch.cat([x1,u4],dim=1)

		x9 = self.conv9(c4)
		# x10 = self.conv10(c4)
		x_cls = self.conv_cls(x9)
		# x_vec = self.conv_vec(x10)
		# output = torch.cat((x_vec, x_cls), 1)

		return x_cls