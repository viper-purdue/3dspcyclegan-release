from __future__ import print_function
import torch
import numpy as np
from PIL import Image
import inspect
import re
import numpy as np
import os
import collections
import skimage.io as io


# Converts a Tensor into a Numpy array
# |imtype|: the desired type of the converted numpy array
def tensor2im(image_tensor, imtype=np.uint8):
    image_numpy = image_tensor[0].cpu().detach().float().numpy()
    if len(image_numpy.shape) < 4:
        image_numpy_temp = np.tile(image_numpy, (3, 1, 1))
        image_numpy_temp = (np.transpose(image_numpy_temp, (1, 2, 0)) + 1) / 2.0 * 255.0
        return image_numpy_temp.astype(imtype)
    
    #3D image
    image_numpy_temp = np.tile(image_numpy[0,0,:,:], (3, 1, 1))
    image_numpy_temp = (np.transpose(image_numpy_temp, (1, 2, 0)) + 1) / 2.0 * 255.0
    image_numpy = (image_numpy[0,:,:,:] + 1) / 2.0 * 255.0
    return image_numpy_temp.astype(imtype), image_numpy.astype(imtype)


def diagnose_network(net, name='network'):
    mean = 0.0
    count = 0
    for param in net.parameters():
        if param.grad is not None:
            mean += torch.mean(torch.abs(param.grad.data))
            count += 1
    if count > 0:
        mean = mean / count
    print(name)
    print(mean)


def save_image(image_numpy, image_path):

    s = image_numpy.shape
    if len(s) == 3:
        # tiff.imwrite(image_path, image_numpy,image_numpy.shape,(s[0],1,s[1],s[2]),imagej=True)
        io.imsave(image_path, image_numpy, check_contrast=False)
    else:
        image_pil = Image.fromarray(image_numpy)
        image_pil.save(image_path)


def print_numpy(x, val=True, shp=False):
    x = x.astype(np.float64)
    if shp:
        print('shape,', x.shape)
    if val:
        x = x.flatten()
        print('mean = %3.3f, min = %3.3f, max = %3.3f, median = %3.3f, std=%3.3f' % (
            np.mean(x), np.min(x), np.max(x), np.median(x), np.std(x)))


def mkdirs(paths):
    if isinstance(paths, list) and not isinstance(paths, str):
        for path in paths:
            mkdir(path)
    else:
        mkdir(paths)


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)
