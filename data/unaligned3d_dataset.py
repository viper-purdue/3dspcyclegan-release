import os.path
import torchvision.transforms as transforms
from data.base_dataset import BaseDataset
from data.image_folder import make_dataset
from PIL import Image
import PIL
import random
import elasticdeform
import skimage.io as io
import torch
import sys
import numpy as np
from skimage.transform import rescale, resize

class Unaligned3dDataset(BaseDataset):
    def __init__(self, opt):
        self.opt = opt
        self.root = opt.dataroot
        self.dir_A = os.path.join(opt.dataroot, opt.phase + 'A')
        self.dir_B = os.path.join(opt.dataroot, opt.phase + 'B')

        self.A_paths = make_dataset(self.dir_A)
        self.B_paths = make_dataset(self.dir_B)

        self.A_paths = sorted(self.A_paths)
        self.B_paths = sorted(self.B_paths)
        self.A_size = len(self.A_paths)
        self.B_size = len(self.B_paths)
        self.transform = self.get_transform()

        self.paired = opt.serial_batches

    def __getitem__(self, index):
        A_path = self.A_paths[index % self.A_size]
        index_A = index % self.A_size
        if self.paired:
            index_B = index % self.B_size
        else:
            index_B = random.randint(0, self.B_size - 1)
        B_path = self.B_paths[index_B]
        #print('(A, B) = (%d, %d)' % (index_A, index_B))
        
        A_img = io.imread(A_path)
        B_img = io.imread(B_path)
        # print(A_img.shape, B_img.shape)
        if self.opt.flip:
            if random.randint(0, 1):
                A_img = A_img[::-1,:,:].copy()
                B_img = B_img[::-1,:,:].copy()
            if random.randint(0, 1):
                A_img = A_img[:,::-1,:].copy()
                B_img = B_img[:,::-1,:].copy()
            if random.randint(0, 1):
                A_img = A_img[:,:,::-1].copy()
                B_img = B_img[:,:,::-1].copy()

        # print(A_path)
        if self.opt.elastic_transform:
            print('using elastic transform')
            points = 4
            sigma = 1
            if 'wsm' in A_path:
                points = 10
                sigma = 1
            elif 'immu' in A_path:
                points = 4
                sigma = 1
            elif 'sherry' in A_path:
                points = 5
                sigma = 2
            elif 'f44' in A_path:
                points = 4
                sigma = 4
            elif '15_047' in A_path:
                points = 4
                sigma = 4
            elif '15_0418' in A_path:
                points = 8
                sigma = 4
            elif 'intestine' in A_path:
                points = 4
                sigma = 4
            elif 'thn' in A_path:
                points = 8
                sigma = 3
            A_img = elasticdeform.deform_random_grid(A_img,points=points, 
                                    sigma=sigma, axis=(1, 2), 
                                    order=0, mode='constant')

        # print(A_img.shape, B_img.shape)
        if A_img.shape[0] == 16:
            # pad to 32
            print('padding images due to small z stack')
            A_img = np.pad(A_img, (8, 8), 'symmetric')
            B_img = np.pad(B_img, (8, 8), 'symmetric')
        # A_img = np.uint8(resize(A_img, (64, 128, 128), order=0, preserve_range=True))
        # B_img = np.uint8(resize(B_img, (64, 128, 128), order=1, preserve_range=True))
        # A_img = np.transpose(A_img, (1,2,0))    # tranpose to (Y,X,Z) during training
        # B_img = np.transpose(B_img, (1,2,0))    # tranpose to (Y,X,Z) during training
        # A_img = A_img[:,:,:,np.newaxis]
        # B_img = B_img[:,:,:,np.newaxis]

        # A_img = Image.open(A_path).convert('RGB')
        # B_img = Image.open(B_path).convert('RGB')
        A_ten = torch.FloatTensor(A_img)
        B_ten = torch.FloatTensor(B_img)
        
        
        A = transforms.functional.normalize(A_ten,[127.5]*A_ten.shape[0],[127.5]*A_ten.shape[0])
        B = transforms.functional.normalize(B_ten,[127.5]*B_ten.shape[0],[127.5]*B_ten.shape[0])
        # A = self.transform(A_img)
        # B = self.transform(B_img)
        
        if self.opt.direction == 'BtoA':
            input_nc = self.opt.output_nc
            output_nc = self.opt.input_nc
        else:
            input_nc = self.opt.input_nc
            output_nc = self.opt.output_nc

        """
        if input_nc == 1:  # RGB to gray
            tmp = A[0, ...] * 0.299 + A[1, ...] * 0.587 + A[2, ...] * 0.114
            A = tmp.unsqueeze(0)

        if output_nc == 1:  # RGB to gray
            tmp = B[0, ...] * 0.299 + B[1, ...] * 0.587 + B[2, ...] * 0.114
            B = tmp.unsqueeze(0)
        """
        
        return {'A': A.view(1,A.shape[0],A.shape[1],A.shape[2]), 'B': B.view(1,B.shape[0],B.shape[1],B.shape[2]),
                'A_paths': A_path, 'B_paths': B_path}

    def __len__(self):
        return max(self.A_size, self.B_size)

    def name(self):
        return 'UnalignedDataset3d'
        
    def get_transform(self):
        transform_list = [transforms.ToTensor(),
                           transforms.Normalize((0.5), (0.5))]
        return transforms.Compose(transform_list)
